
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
<?php

	include("connect2.php"); 	
	
	$link=Connection();

	$result=mysql_query("SELECT * FROM `ruchLog` ORDER BY `timeStamp` DESC",$link);
?>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">MyHome</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="#">Panel główny</a></li>
            <li><a href="#about">Monitoring</a></li>
             <li class="dropdown active">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Czujniki <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    
                    <li><a href="ruch.php">Czujnik ruchu</a></li>
                  
					
				
                  </ul>
                </li>
			 <li><a href="#contact">Oświetlenie</a></li>
			 <li><a href="#contact">Dostęp</a></li>
			 <li><a href="#contact">Bezpieczeństwo</a></li>
			 <li><a href="#contact">Powiadomienia</a></li>
			 <li><a href="#contact">Ustawienia</a></li>
			 <li><a href="#">Pomoc techniczna</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <div class="starter-template">
        <h1>Czujnik ruchu</h1>

   <table class="table table-hover">
		<thead>
		<tr>
			<td><b>&nbsp;Data wykrycia ruchu&nbsp;</b></td>
			
			
		</tr>
		</thead>
      <?php 
		  if($result!==FALSE){
		    while($row = mysql_fetch_array($result)) {
		        printf("<tr><td> &nbsp;%s </td><td>", 
		           $row["timeStamp"]);
		     }
		     mysql_free_result($result);
		     mysql_close();
		  }
      ?>

   </table>
   
   <button type="button" class="btn btn-danger">Wyczyść pomiary</button>
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
